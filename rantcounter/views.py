from django.shortcuts import render
from django.forms import modelform_factory
from django.forms import inlineformset_factory
from django.shortcuts import redirect

from django.http import HttpResponse
from django.views.decorators.http import require_POST

from .models import Rant
from .models import RantEntry

from .forms import RantEntryInlineFormset

from dejafeddb.utils import reverse_lazy


def rant_detail(request, rant_id):
    context = {}
    form_factory = inlineformset_factory(Rant, RantEntry,
                                         fields=('label',),
                                         formset=RantEntryInlineFormset,
                                         can_delete_extra=False,
                                         max_num=1)

    obj = Rant.objects.get(id=rant_id)

    if request.method == 'POST':
        form = form_factory(request.POST, instance=obj)
        if form.is_valid():
            form.save()

    form = form_factory(instance=obj)

    others = Rant.objects.exclude(id=rant_id).order_by('-created')[:10]
    context['form'] = form
    context['obj'] = obj
    context['form_target'] = obj.get_absolute_url()
    context['is_rants'] = True
    context['others'] = others
    return render(request, 'rant_detail.html', context)


@require_POST
def rant_detail_view(request, rant_id):
    obj = Rant.objects.get(id=rant_id)
    return HttpResponse(obj.render())


def rant(request):
    context = {
        'is_rants': True,
    }
    form_factory = modelform_factory(Rant, fields=('title',))
    if request.method == 'POST':
        form = form_factory(request.POST)
        if form.is_valid():
            obj = form.save()
            return redirect(obj)
    else:
        form = form_factory()

    context['form'] = form
    context['form_target'] = reverse_lazy('rant')
    context['others'] = Rant.objects.order_by('-created')[:10]
    return render(request, 'rant.html', context)


@require_POST
def update_rant(request, rantentry_id):
    obj = RantEntry.objects.get(id=rantentry_id)
    obj.count += 1
    obj.save()

    return HttpResponse(obj.render(True))
