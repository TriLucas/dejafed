from django.urls import path

from . import views
urlpatterns = [
    path('', views.rant,
         name='rant'),
    path('<str:rant_id>/', views.rant_detail,
         name='rant_detail'),
    path('<str:rant_id>/xhr_view/', views.rant_detail_view,
         name='rant_detail_view'),
    path('<str:rantentry_id>/xhr_update/', views.update_rant,
         name='update_rant_entry'),
]
