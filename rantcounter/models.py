import uuid

from django.db import models
from django.template import loader

from dejafeddb.utils import reverse_lazy


class RantEntry(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['label', 'rant'],
                                    name='distinct_rants'),
        ]

    count = models.IntegerField(default=0)
    label = models.CharField(null=False, default=None, max_length=200)
    rant = models.ForeignKey('Rant', on_delete=models.DO_NOTHING)

    def render(self, view_only=False):
        template = loader.get_template('xhr/rant_entry.html')
        return template.render({'entry': self, 'view_only': view_only})

    def get_absolute_url(self):
        return reverse_lazy('update_rant_entry', args=(self.pk,))


class Rant(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(null=False, max_length=200)
    created = models.DateTimeField(auto_now=True, null=False)

    def get_absolute_url(self, view=False):
        if not view:
            return reverse_lazy('rant_detail', args=(self.pk,))
        return reverse_lazy('rant_detail_view', args=(self.pk,))

    def render(self, view_only=False):
        template = loader.get_template('xhr/rant.html')
        return template.render({'rant': self, 'view_only': view_only})
