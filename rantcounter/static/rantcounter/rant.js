"use strict";

window.addEventListener("load", event => {
  let getCookie = name => {
    let cookieValue = null;
    if (document.cookie && document.cookie !== "") {
      const cookies = document.cookie.split(";");
      for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i].trim();
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + "=")) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  };

  let csrftoken = getCookie("csrftoken");

  let singleCallback = form => {
    return function(e) {
      let target = form.querySelector(".rant_entry_content");
      target.innerHTML = this.responseText;
    };
  };

  let fireXhr = (form, callback) => {
    let xhr = new XMLHttpRequest();
    let data = new FormData(form);

    xhr.onload = callback;

    xhr.open("POST", form.action);
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.send(data);
  };

  let addClickers = () => {
    let rantForms = document.querySelectorAll(".rant_entry_form");

    for (let form of rantForms) {
      let btn = form.querySelector(".rant_entry_inc");
      btn.addEventListener("click", clickEvent => fireXhr(
        form,
        singleCallback(form)
      ));
    }
  };

  addClickers();

  let fullRantDiv = document.querySelector(".rant_contents");
  let fullRantForm = fullRantDiv.querySelector(".rant_form_xhr_full");

  let refreshTimeout = null;
  let nextTimeout = callback => {
    if (refreshTimeout != null) {
      window.clearTimeout(refreshTimeout);
      refreshTimeout = null;
    }
    refreshTimeout = window.setTimeout(callback, 5000);
  };

  let autoCallback = div => {
    return function(e) {
      div.innerHTML = this.responseText;
      let nextForm = div.querySelector(".rant_form_xhr_full");
      addClickers();
      nextTimeout(() => fireXhr(nextForm, autoCallback(div)));
    };
  };

  nextTimeout(() => fireXhr(fullRantForm, autoCallback(fullRantDiv)));
});
