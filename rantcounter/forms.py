from django import forms

from .models import RantEntry


class RantEntryInlineFormset(forms.BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queryset = RantEntry.objects.none()
