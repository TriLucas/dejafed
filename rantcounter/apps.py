from django.apps import AppConfig


class RantcounterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rantcounter'
