from django.contrib import admin

from .models import Rant
from .models import RantEntry


class RantItemAdmin(admin.TabularInline):
    model = RantEntry


@admin.register(Rant)
class RantAdmin(admin.ModelAdmin):
    readonly_fields = 'created',
    inlines = RantItemAdmin,
    list_display = 'title', 'created'
