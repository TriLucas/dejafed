from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('dejafeddb.urls')),
    path('rant/', include('rantcounter.urls')),
    path('admin/', admin.site.urls),
]
