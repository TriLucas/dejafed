import riotwatcher

from riotwatcher import ApiError

from datetime import datetime

from django.contrib import admin
from django.template import loader
from django.db import models

from django.conf import settings
from django.utils import timezone

from .api_utils import RiotApiMixin
from .api_utils import SubmodelMapping
from .api_utils import RiotApi
from .api_utils import api_timestamp
from .api_utils import CachedApiResponse

from dejafeddb.atomiclock import NamedAtomicLockContext

import logging

import json


logger = logging.getLogger('dejafeddb')


class JsonField(models.TextField):
    _capture_paths = []

    def to_python(self, value):
        if value is None:
            return value

        return json.loads(value)

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value

        return json.loads(value)

    def _fill_value(self, keys, source, target, level=0):
        current_key = keys[0]
        if len(keys) == 1:
            target[current_key] = source.get(current_key, None)
        else:
            target.setdefault(current_key, {})
            self._fill_value(keys[1:], source[current_key],
                             target[current_key], level=level + 1)

    def _get_prep_value(self, value):
        if value is None or value == []:
            return None

        res_value = {}
        if len(self._capture_paths) > 0:
            for c_path in self._capture_paths:
                self._fill_value(c_path, value, res_value)

            return json.dumps(res_value)
        return json.dumps(value)

    def get_prep_value(self, value):
        return self._get_prep_value(value)

    def get_db_prep_value(self, value, connection, prepared=False):
        return self._get_prep_value(value)


class CapturedApiData(JsonField):
    def __init__(self, capture_paths=[], *args, **kwargs):
        self._capture_paths = capture_paths
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if self._capture_paths != []:
            kwargs['capture_paths'] = self._capture_paths

        return name, path, args, kwargs


class Champion(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    def image_url(self):
        cacher = CachedApiResponse()
        meta = cacher.get_cached(RiotApi.data_dragon.versions_for_region,
                                 ('euw1',))

        version = meta['n']['champion']
        cdn_url = meta['cdn']

        asset_data = cacher.get_cached(RiotApi.data_dragon.champions,
                                       (version,))
        champ_data = asset_data['data'][self.name]
        f_name = champ_data['image']['full']

        return f'{cdn_url}/{version}/img/champion/{f_name}'


class SummonerMatchManager(models.Manager):
    def team_a(self):
        return super().all().filter(team='100')

    def team_b(self):
        return super().all().filter(team='200')


class SummonerMatch(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['match', 'summoner'],
                                    name='distinct_summoner_matches'),
        ]

    objects = SummonerMatchManager()

    role = models.TextField()
    team = models.TextField()
    win = models.BooleanField()
    kills = models.IntegerField()
    deaths = models.IntegerField()
    assists = models.IntegerField()
    match = models.ForeignKey('Match', on_delete=models.RESTRICT)
    summoner = models.ForeignKey('Summoner', on_delete=models.RESTRICT)
    champion = models.ForeignKey('Champion', on_delete=models.DO_NOTHING)

    item0 = models.IntegerField(null=True, default=None)
    item1 = models.IntegerField(null=True, default=None)
    item2 = models.IntegerField(null=True, default=None)
    item3 = models.IntegerField(null=True, default=None)
    item4 = models.IntegerField(null=True, default=None)
    item5 = models.IntegerField(null=True, default=None)
    item6 = models.IntegerField(null=True, default=None)

    summoner_level = models.IntegerField(null=True, default=None)
    champ_level = models.IntegerField(null=True, default=None)

    def render(self):
        template = loader.get_template('roster_entry.html')
        return template.render({'summoner': self.summoner, 'sm': self})

    def items(self):
        data = []

        cacher = CachedApiResponse()
        meta = cacher.get_cached(RiotApi.data_dragon.versions_for_region,
                                 ('euw1',))

        version = meta['n']['item']
        cdn_url = meta['cdn']

        asset_data = cacher.get_cached(RiotApi.data_dragon.items,
                                       (version,))

        items = [self.item0, self.item1, self.item2, self.item3, self.item4,
                 self.item5, self.item6]

        template = loader.get_template('item_image.html')

        for item in items:
            if item is None:
                continue

            try:
                item_data = asset_data['data'][f'{item}']
                img_name = item_data['image']['full']

                url = f'{cdn_url}/{version}/img/item/{img_name}'
                tooltip = item_data['description']
                alt = item_data['name']

                data.append(template.render({'url': url, 'tooltip': tooltip,
                                             'alt': alt}))
            except KeyError:
                pass

        return data


class MatchManager(RiotApiMixin, models.Manager):
    def identifier_field(self, identifier):
        return {'id': identifier}

    def get_riot_api_data(self, identifier, region='europe'):
        super().get_riot_api_data()

        return RiotApi.match.by_id(region, identifier)

    def api_mapping(self, data):
        return {
            'id': data['metadata']['matchId'],
            'mode': data['info']['gameMode'],
            'date': api_timestamp(data['info']['gameStartTimestamp']),
            'raw_data': data,
        }


class Match(models.Model):
    objects = MatchManager()

    id = models.CharField(primary_key=True, max_length=20)
    mode = models.TextField(default=None, null=False)
    date = models.DateTimeField()
    raw_data = CapturedApiData(
        null=True,
        default=None,
        capture_paths=[
            ['info', 'participants'],
            ['metadata'],
        ],
    )

    def is_populated(self, of_summoners=[]):
        _filter = models.Q()
        if of_summoners:
            _filter = models.Q(pk__in=[p.pk for p in of_summoners])

        pre_populated = self.summoner_set.filter(_filter).distinct().count()

        vgl = len(of_summoners) if of_summoners else 10

        return pre_populated == vgl

    def populate(self, with_summoners=[]):
        if self.is_populated(with_summoners):
            return

        puuids = [p.puuid for p in with_summoners]
        logger.debug(f'puuids: {puuids}')

        existing_sms = self.summonermatch_set.filter(
            models.Q(summoner__puuid__in=puuids) if puuids else models.Q(),
        ).values_list('summoner__puuid', flat=True)

        existing_players = Summoner.objects.filter(
            puuid__in=[p['puuid']
                       for p in self.raw_data['info']['participants']],
        )

        existing_p_puuids = existing_players.values_list('puuid', flat=True)
        logger.debug(f'Existing: {existing_sms}')

        # Find all summoners not yet linked to this match
        summoners_needed = [
            p for p in self.raw_data['info']['participants']
            if (not puuids or p['puuid'] in puuids)
            and p['puuid'] not in existing_sms
        ]

        logger.debug(
            f'Summoners to populate: {[p["puuid"] for p in summoners_needed]}',
        )

        logger.debug(f'existing players: {existing_players}')
        # Create all non existent summoners
        for participant in summoners_needed:
            if participant['puuid'] not in existing_p_puuids:
                obj, created = Summoner.objects.get_or_create(
                    puuid=participant['puuid'],
                )

        p_dict = {x.puuid: x for x in existing_players}

        SummonerMatch.objects.bulk_create(
            SummonerMatch(
                match=self,
                summoner=p_dict[participant['puuid']],
                win=participant['win'],
                deaths=participant['deaths'],
                assists=participant['assists'],
                kills=participant['kills'],
                role=' '.join([participant['role'],
                               participant['teamPosition']]),
                team=participant['teamId'],
                champion=Champion.objects.get(
                    models.Q(id=participant['championId'])
                    | models.Q(name=participant['championName']),
                ),
                champ_level=participant['champLevel'],
                summoner_level=participant['summonerLevel'],
                item0=participant['item0'],
                item1=participant['item1'],
                item2=participant['item2'],
                item3=participant['item3'],
                item4=participant['item4'],
                item5=participant['item5'],
                item6=participant['item6'],
            ) for participant in summoners_needed
        )


class SummonerManager(RiotApiMixin, models.Manager):
    def identifier_field(self, identifier):
        if identifier is not None:
            _data = self.get_riot_api_data(identifier, 'euw1')
            return {'id': _data['id']}
        return {}

    def get_riot_api_data(self, identifier, region='euw1', **kwargs):
        super().get_riot_api_data()

        fnc_mapping = {
            'puuid': RiotApi.summoner.by_puuid,
            'id': RiotApi.summoner.by_id,
        }
        if identifier is not None:
            logger.info(f'Explicitly fetching API data for {identifier}')
            try:
                return RiotApi.summoner.by_name(region, identifier)
            except ApiError as err:
                if err.response.status_code in {403, 404}:
                    raise self.model.DoesNotExist
                raise err

        for key in fnc_mapping:
            if key in kwargs:
                return fnc_mapping[key](region, kwargs[key])

    def api_mapping(self, data):
        subname = SubmodelMapping()
        subname['name'] = data['name']
        subname['revision_date'] = api_timestamp(data['revisionDate'])
        return {
            'summonername_set': subname,
            'id': data['id'],
            'accountId': data['accountId'],
            'puuid': data['puuid'],
        }

    def get_queryset(self):
        qset = super().get_queryset()
        cur_name = SummonerName.objects.filter(
            for_summoner=models.OuterRef('pk'),
        ).order_by('-revision_date')
        return qset.annotate(name=models.Subquery(cur_name.values('name')[:1]))


class Summoner(models.Model):
    objects = SummonerManager()

    id = models.CharField(default=None, null=False, primary_key=True,
                          max_length=200)
    accountId = models.CharField(default=None, null=False, unique=True,
                                 max_length=200)
    puuid = models.CharField(default=None, null=False, unique=True,
                             max_length=200)
    raw_past_matches = JsonField(null=True, default=None)
    raw_past_matches_fetched = models.DateTimeField(null=True, default=None)
    live_game_data = CapturedApiData(null=True, default=None,
                                     capture_paths=[['participants'],
                                                    ['gameId'],
                                                    ['gameMode'],
                                                    ['gameStartTime']])
    live_game_data_fetched = models.DateTimeField(null=True, default=None)

    matches = models.ManyToManyField(Match, through=SummonerMatch)

    @admin.display(ordering='name', description='Current Summonername')
    def display_name(self):
        return self.__str__()

    def __str__(self):
        if hasattr(self, 'name'):
            return f'{self.name}'
        try:
            return f'{self.summonername_set.latest("-revision_date").name}'
        except models.ObjectDoesNotExist:
            return f'MISSING SUMMONER NAMES FOR PUUID {self.puuid}'

    def api_matches(self, step=100, start=0, start_time=None):
        return RiotApi.match.matchlist_by_puuid(
            'europe',
            self.puuid,
            count=step,
            start=start,
            start_time=int(start_time.timestamp()) if start_time else None,
        )

    def _find_first_of_in(self, list_a, list_b):
        idx = 0
        while True:
            try:
                needle = list_a[idx]
                try:
                    return list_b.index(needle)
                except ValueError:
                    idx += 1
            except IndexError:
                return -1

    def _merge_in_order(self, list_a, list_b):
        idx_p = 0
        idx_s = 0

        first_a_in_b = self._find_first_of_in(list_a, list_b)
        first_b_in_a = self._find_first_of_in(list_b, list_a)

        if first_b_in_a > first_a_in_b:
            first_prim_in_sec = first_b_in_a
            primary = list_a
            secondary = list_b
        else:
            first_prim_in_sec = first_a_in_b
            primary = list_b
            secondary = list_a

        while idx_p <= len(primary) and idx_s <= len(secondary):
            while idx_p < first_prim_in_sec:
                yield primary[idx_p]
                idx_p += 1

            if idx_s < len(secondary) and secondary[idx_s] not in primary:
                value = secondary[idx_s]
                yield value
                idx_s += 1
            elif idx_p < len(primary):
                value = primary[idx_p]
                yield value
                idx_p += 1
                if value in secondary:
                    idx_s += 1
            else:
                break

    def expired_matches(self):
        fetched = self.raw_past_matches_fetched

        aware_now = timezone.now()
        aware_expired = aware_now - settings.RAW_MATCHES_EXPIRATION

        return fetched is None or fetched < aware_expired

    def expired_live(self):
        fetched = self.live_game_data_fetched

        aware_now = timezone.now()
        aware_expired = aware_now - settings.RAW_LIVEDATA_EXPIRATION

        return fetched is None or fetched < aware_expired

    def expired_data(self):
        return self.expired_live() or self.expired_matches()

    def last_matches_cached(self, force_refetch=False, n_matches=None):
        if self.expired_matches() or force_refetch:
            logger.debug(
                f'!!! Match list for {self} expired, forced: '
                f'{force_refetch}. Refetching.',
            )
            n_matches = n_matches or settings.ENSURE_PRISTINE_MATCHES

            start = 0
            result = []

            _ref = self.raw_past_matches or []

            while True:
                _last = self.api_matches(start=start)
                start += len(_last)
                result += _last

                prev_and_new = (len(result) >= n_matches and len(_ref) != 0)
                if len(_last) == 0 or prev_and_new:
                    break

            if not set(result).issubset(set(_ref)):
                logger.warning(f'Attemping to merge {len(result)} matches for'
                               f' {self}')
                self.raw_past_matches = list(self._merge_in_order(
                    result,
                    _ref,
                ))
            self.raw_past_matches_fetched = timezone.now()
            self.save()

        return self.raw_past_matches

    def live_data_cached(self, region, force_refetch=False):
        if self.expired_live() or force_refetch:
            logger.debug(
                f'!!! Lifedata for {self} expired, forced: '
                f'{force_refetch}. Refetching.',
            )
            api_exception = None

            live_data = None
            try:
                live_data = RiotApi.spectator.by_summoner(region, self.id)
                logger.info(f'!!! riotapi.specator returned {live_data}')
            except riotwatcher.ApiError as api_exc:
                logger.info('!!! riotapi.specator returned NONE.')
                api_exception = api_exc
            finally:
                self.live_game_data = live_data
                self.live_game_data_fetched = timezone.now()
                self.save()

            if api_exception is not None:
                raise api_exception

        return self.live_game_data


class SummonerName(models.Model):
    name = models.CharField(default=None, null=False, max_length=200)
    for_summoner = models.ForeignKey(Summoner, on_delete=models.RESTRICT)
    revision_date = models.DateTimeField(
        default=timezone.make_aware(datetime.fromtimestamp(0)),
    )


class Region(models.Model):
    platform = models.CharField(max_length=4)
    region = models.CharField(max_length=8)


class ApiLimitManager(models.Manager):
    _lock = NamedAtomicLockContext(
        settings.NAMED_ATOMIC_API_LOCK,
        settings.NAMED_LOCKS_DIRECTORY,
        settings.NAMED_LOCK_MAX_AGE,
    )

    def current(self):
        with self._lock:
            obj, _ = self.get_or_create(pk=1)
        return obj.retry_at

    def increase_to(self, value):
        with self._lock:
            obj, _ = self.get_or_create(pk=1)

        if obj.retry_at is None or value > obj.retry_at:
            obj.retry_at = value
            obj.save()
        return obj.retry_at

    def reset(self):
        with self._lock:
            obj, _ = self.get_or_create(pk=1)

        obj.retry_at = None
        obj.save()


class ApiLimit(models.Model):
    objects = ApiLimitManager()

    retry_at = models.DateTimeField(null=True, default=None)


class Switches(models.Model):
    name = models.CharField(null=True, default=None, max_length=200)
    switch = models.BooleanField(null=False, default=True)

    def __str__(self):
        return f'{self.name} : {self.switch}'
