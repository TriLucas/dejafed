from django.urls import path

from . import views

urlpatterns = [
    path('', views.live, name='index'),
    path('match/<str:match_id>', views.match, name='match'),
    path('current/<str:summoner_id>', views.current_live_game,
         name='xhr_live_id'),
    path('summoners/<str:query>', views.summoner_suggestions,
         name='xhr_summoner_suggestions'),
    path('currentencounters/', views.xhr_current_encounter,
         name='xhr_current_encounter'),
    path('encounters/', views.encounters, name='encounters'),
    path('summonerencounters/', views.xhr_summoners_encounters,
         name='xhr_summoners_encounters'),
    path('colorsession/', views.set_colormode, name='set_colormode'),
]
