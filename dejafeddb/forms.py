import uuid
import logging

from riotwatcher import ApiError

from .models import Summoner
from django import forms
from django.template import loader
from django.conf import settings

from .utils import reverse_lazy

logger = logging.getLogger('dejafeddb')
MAX_ENCOUNTERS = [5, 10, 20, 50, 100]


class XHRFormMixin:
    template_file = 'xhr/xhr_form.html'

    FORM_ID = 'summoner_selection'
    FORM_CLASS = None
    FORM_ACTION = None

    ADDITIONAL_HIDDEN = []

    def __init__(self, *args, additional_hidden=None, form_id=None,
                 form_class=None, form_action=None, form_action_view_name=None,
                 request=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._request = request
        self._additional_hidden = additional_hidden or self.ADDITIONAL_HIDDEN

        self._form_id = form_id or self.FORM_ID
        self._form_class = form_class or self.FORM_CLASS
        if form_action_view_name is None:
            self._form_action = form_action or self.FORM_ACTION
        else:
            self._form_action = reverse_lazy(form_action_view_name)

    def extra_scripts(self):
        return []

    def additional_hidden(self):
        for item in self._additional_hidden:
            _id = item.get('id', None)
            value = item.get('value', None)
            name = item.get('name', None)
            classname = item.get('class', None)
            yield _id, value, name, classname

    def make_context(self):
        return {
            'form_id': self._form_id,
            'form_class': self._form_class,
            'form_action': self._form_action,
            'additional_hidden': self.additional_hidden(),
            'form': self,
        }

    def as_html(self):
        template = loader.get_template(self.template_file)
        return template.render(self.make_context(), request=self._request)


class XhrSubmitMixin(XHRFormMixin):
    def extra_scripts(self):
        return super().extra_scripts() + ['dejafeddb/async.js']


class AutoRefreshMixin(XHRFormMixin):
    template_file = 'xhr/xhr_autorefresh_form.html'

    def __init__(self, *args, cls_lastmatch=None, cls_currentmatch=None,
                 **kwargs):
        super().__init__(*args, **kwargs)
        self._cls_lastmatch = cls_lastmatch
        self._cls_currentmatch = cls_currentmatch

    def extra_scripts(self):
        _scripts = super().extra_scripts()
        if self.is_valid():
            _scripts += ['dejafeddb/async.js']
            if self.cleaned_data.get('auto_refresh'):
                _scripts += ['dejafeddb/auto_live.js']
        return _scripts

    def additional_hidden(self):
        for _id, value, name, classname in super().additional_hidden():
            yield _id, value, name, classname

        viewed_summoner = None
        if self.is_valid():
            viewed_summoner = self.cleaned_data.get('summoner', None)

        if viewed_summoner is not None:
            yield (None, settings.AUTO_REFRESH_TIME, None,
                   'xhr_autorefresh_time')
            yield (None, reverse_lazy('xhr_live_id',
                                      args=(viewed_summoner.id,)),
                   None, 'xhr_autorefresh_url')
            yield (None, self.cleaned_data['ref_match_id'], None,
                   'xhr_autorefresh_refid')

    def make_context(self):
        context = super().make_context()
        _show = self.is_valid() and self.cleaned_data.get('auto_refresh')
        context.update({
            'refresh_view': _show,
        })
        return context

    def clean(self):
        cleaned_data = super().clean() or self.cleaned_data

        match_select = cleaned_data.get('match_select', None)
        summoner = cleaned_data.get('summoner', None)

        if summoner is not None and match_select is not None:
            if int(match_select) == 1:
                klass = self._cls_lastmatch
            else:
                klass = self._cls_currentmatch

            cleaned_data['resolver'] = klass(
                cleaned_data['summoner'],
                limit_encounters=cleaned_data['limit_encounters'],
                auto_refresh=cleaned_data['auto_refresh'],
            )

            try:
                _ref_id = cleaned_data['resolver'].match_data.live_id
                cleaned_data['ref_match_id'] = _ref_id
            except (ApiError, cleaned_data['resolver'].NoMatchFound):
                cleaned_data['ref_match_id'] = None

        self.cleaned_data = cleaned_data

        return cleaned_data


class SuggestionXhrRenderMixin(XHRFormMixin):
    SUGGESTION_FIELDS = []
    ADDITIONAL_HIDDEN = []

    SUGGESTION_TARGET_CLS_PREFIX = 'suggestion_target_'
    DATALIST_ID_PREFIX = 'id_datalist_'
    DATALIST_TARGET_CLS_PREFIX = 'suggestion_datalist_'

    URL_INP_ID = 'id_xhr_suggestions_url'
    URL_INP_TARGET = 'xhr_summoner_suggestions'

    def __init__(self, *args, url_input_id=None, url_input_target=None,
                 **kwargs):
        super().__init__(*args, **kwargs)
        self._id_suggestions_url_input = url_input_id or self.URL_INP_ID
        self._target_suggestions_url = url_input_target or self.URL_INP_TARGET

    def extra_scripts(self):
        return super().extra_scripts() + ['dejafeddb/suggestions.js']

    def additional_hidden(self):
        for _id, value, name, classname in super().additional_hidden():
            yield _id, value, name, classname

        yield (
            None,
            len(self.SUGGESTION_FIELDS),
            'num_targets',
            'n_suggestion_targets',
        )

        for idx, item in enumerate(self.SUGGESTION_FIELDS):
            bound_field = self.fields[item].get_bound_field(self, item)
            yield (
                None,
                bound_field.auto_id,
                None,
                f'{self.SUGGESTION_TARGET_CLS_PREFIX}{idx}',
            )
            yield (
                None,
                f'{self.DATALIST_ID_PREFIX}{item}',
                None,
                f'{self.DATALIST_TARGET_CLS_PREFIX}{idx}',
            )

        yield (
            self._id_suggestions_url_input,
            reverse_lazy(self._target_suggestions_url,
                         args=('SUMMONER_QUERY',)),
            None,
            None,
        )

    def datalist_items(self):
        for item in self.SUGGESTION_FIELDS:
            yield f'{self.DATALIST_ID_PREFIX}{item}'


class SummonerMatchSelection(SuggestionXhrRenderMixin, AutoRefreshMixin,
                             forms.Form):
    SUGGESTION_FIELDS = [
        'name',
    ]

    name = forms.CharField(
        label='Summoner name',
        max_length=200,
        widget=forms.TextInput(attrs={'autocomplete': 'off'}),
    )
    puuid = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )
    match_select = forms.ChoiceField(
        widget=forms.RadioSelect(attrs={'onclick': 'toggleAutoRefresh(this)'}),
        choices=[('0', 'current'), ('1', 'last')],
        initial='0',
    )
    auto_refresh = forms.BooleanField(label='Autorefresh',
                                      initial=True,
                                      required=False)
    limit_encounters = forms.IntegerField(
        widget=forms.Select(
            choices=list(zip(MAX_ENCOUNTERS, MAX_ENCOUNTERS)),
        ),
        initial=MAX_ENCOUNTERS[2],
        label='Displayed encounters',
    )

    def clean(self):
        cleaned_data = super().clean() or self.cleaned_data
        match_type = cleaned_data['match_select']

        if int(match_type) == 1:
            cleaned_data['auto_refresh'] = False

        self.cleaned_data = cleaned_data

        _data = self.data.copy()
        _data.update({'puuid': cleaned_data.get('puuid', None)})
        setattr(self, 'data', _data)

        return cleaned_data

    def clean_puuid(self):
        puuid = self.cleaned_data.get('puuid')
        name = self.cleaned_data.get('name')

        t_obj = None

        if puuid:
            try:
                t_obj = Summoner.objects.get(puuid=puuid)
            except Summoner.DoesNotExist:
                pass

        if name:
            if not puuid or t_obj is None or t_obj.name != name:
                try:
                    t_obj, _ = Summoner.objects.get_or_create(name)
                    puuid = t_obj.puuid
                except Summoner.DoesNotExist:
                    self.add_error(
                        'name',
                        f'Summoner with name {name} does not exist!',
                    )

        self.cleaned_data['summoner'] = t_obj
        return puuid


class CompileEncountersForm(SuggestionXhrRenderMixin, XhrSubmitMixin,
                            forms.Form):
    FORM_CLASS = 'xhr_form'
    SUGGESTION_FIELDS = [
        'name_a',
        'name_b',
    ]
    ADDITIONAL_HIDDEN = [
        {'value': reverse_lazy('xhr_summoners_encounters'),
         'class': 'xhr_url'},
        {'value': 'encounter_container', 'class': 'xhr_target'},
        {'value': 'xhr_spinner', 'class': 'xhr_spinner'},
    ]

    name_a = forms.CharField(
        label='Summoner 1 name',
        max_length=200,
        widget=forms.TextInput(attrs={'autocomplete': 'off'}),
    )
    puuid_a = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )
    name_b = forms.CharField(
        label='Summoner 2 name',
        max_length=200,
        widget=forms.TextInput(attrs={'autocomplete': 'off'}),
    )
    puuid_b = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )
    limit_encounters = forms.IntegerField(
        widget=forms.Select(
            choices=list(zip(MAX_ENCOUNTERS, MAX_ENCOUNTERS)) + [(0, 'all')],
        ),
        initial=MAX_ENCOUNTERS[2],
        label='Displayed encounters',
    )

    def make_context(self):
        ctx = super().make_context()
        if self.is_valid():
            ctx['auto_post'] = True
        return ctx

    def __init__(self, *args, view=None, **kwargs):
        self._view = view
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean() or self.cleaned_data
        self.cleaned_data = cleaned_data

        _data = self.data.copy()
        _data.update({'puuid_a': cleaned_data['puuid_a']})
        _data.update({'puuid_b': cleaned_data['puuid_b']})
        setattr(self, 'data', _data)

        return cleaned_data

    def clean_limit_encounters(self):
        value = int(self.cleaned_data.get('limit_encounters'))
        if value == 0:
            return None
        return value

    def _clean_puuid(self, puuid_field, name_field, summoner_field):
        puuid = self.cleaned_data.get(puuid_field)
        name = self.cleaned_data.get(name_field)

        t_obj = None

        if puuid:
            try:
                t_obj = Summoner.objects.get(puuid=puuid)
            except Summoner.DoesNotExist:
                pass

        if name:
            if not puuid or t_obj is None or t_obj.name != name:
                t_obj, _ = Summoner.objects.get_or_create(name)
                puuid = t_obj.puuid

        self.cleaned_data[summoner_field] = t_obj
        return puuid

    def clean_puuid_a(self):
        return self._clean_puuid('puuid_a', 'name_a', 'summoner_a')

    def clean_puuid_b(self):
        return self._clean_puuid('puuid_b', 'name_b', 'summoner_b')

    def direct_link(self):
        qdict = {}
        for item in self.fields:
            qdict[item] = self.cleaned_data[item]
        return reverse_lazy(self._view, qdict=qdict)


class MatchEncounterQueryForm(forms.Form):
    def __init__(self, *args, **kwargs):
        id_str = uuid.uuid4().hex
        super().__init__(*args, auto_id='id_{}_for_%s'.format(id_str),
                         **kwargs)
    for_summoner = forms.ModelChoiceField(Summoner.objects.all(),
                                          widget=forms.TextInput())
    with_summoner = forms.ModelChoiceField(Summoner.objects.all(),
                                           widget=forms.TextInput())
    match = forms.CharField(required=False, widget=forms.TextInput())
    limit_encounters = forms.IntegerField(
        widget=forms.Select(
            choices=list(zip(MAX_ENCOUNTERS, MAX_ENCOUNTERS)),
        ),
    )
