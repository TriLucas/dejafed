import logging

from riotwatcher.Handlers import ApiError
from functools import wraps

from .business import DejafedApi
from .business import DejafedDuoEncounter
from .business import DejafedApiLastmatch

from django.conf import settings

from django.template import engines
from django.template.exceptions import TemplateSyntaxError

from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse

from django.views.decorators.http import require_POST

from .forms import SummonerMatchSelection
from .forms import MatchEncounterQueryForm
from .forms import CompileEncountersForm

from .models import Summoner
from .models import Match
from .models import Switches

from .api_utils import api_timestamp

from datetime import datetime

logger = logging.getLogger('dejafeddb')


def construction(fnc):
    @wraps(fnc)
    def wrapper(request, *args, **kwargs):
        switch_obj = Switches.objects.get(name='show_construction')
        is_authenticated = request.user.is_authenticated
        logger.info(f'User authenticated: {is_authenticated} - {request.user}')
        if not switch_obj.switch or is_authenticated:
            return fnc(request, *args, **kwargs)
        return render(request, 'construction.html', {'TQDN': settings.TQDN})
    return wrapper


@construction
def live(request):
    context = {}
    if request.method == 'POST':
        data = request.POST
    else:
        data = request.GET

    if data != {}:
        form = SummonerMatchSelection(request.POST or request.GET,
                                      form_action_view_name='index',
                                      request=request,
                                      cls_currentmatch=DejafedApi,
                                      cls_lastmatch=DejafedApiLastmatch)
        if form.is_valid():
            try:
                resolver = form.cleaned_data['resolver']
                context['match_id'] = resolver.handled_match
                context['viewed'] = form.cleaned_data['summoner']
                context['auto_refresh'] = form.cleaned_data['auto_refresh']

                logger.info('RENDERING BLUE TEAM')
                context['blue'] = resolver.render_blue_team()

                logger.info('RENDERING RED TEAM')
                context['red'] = resolver.render_red_team()

                context['match_data'] = resolver.match_data

            except (ApiError, form.cleaned_data['resolver'].NoMatchFound):
                context['match_data'] = {'live_id': None}
                context['error'] = 'No game found!'
    else:
        form = SummonerMatchSelection(form_action_view_name='index',
                                      request=request)

    context['summoner_form'] = form
    context['refresh_time'] = settings.AUTO_REFRESH_TIME
    context['is_index'] = True
    context['extra_scripts'] = form.extra_scripts()
    return render(request, 'live.html', context)


@construction
def match(request, match_id):
    match_obj = get_object_or_404(Match, id=match_id)
    match_obj.populate()
    context = {'match': match_obj}
    return render(request, 'match.html', context)


def render_datetime(dtime):
    for engine in engines.all():
        try:
            template = engine.from_string('{{ data|localize }}')
            return template.render({
                'data': dtime,
            })
        except TemplateSyntaxError:
            pass


@construction
def current_live_game(request, summoner_id):
    summoner_obj = get_object_or_404(Summoner, id=summoner_id)
    data = {'now': render_datetime(datetime.now().astimezone())}
    try:
        raw_live_data = summoner_obj.live_data_cached('euw1')

        if raw_live_data is not None:
            data['gameId'] = raw_live_data['gameId']
            t_start = api_timestamp(raw_live_data['gameStartTime'])
            if t_start is not None:
                data['start'] = render_datetime(t_start)
    except ApiError:
        pass
    return JsonResponse(data)


@construction
@require_POST
def xhr_current_encounter(request):
    form = MatchEncounterQueryForm(request.POST)
    if form.is_valid():
        if not form.cleaned_data['match']:
            klass = DejafedApi
        else:
            klass = DejafedApiLastmatch

        calculator = klass(
            form.cleaned_data['for_summoner'],
            handled_match=form.cleaned_data['match'],
            limit_encounters=form.cleaned_data['limit_encounters'],
        )
        return HttpResponse(calculator.render_encounters_with(
            form.cleaned_data['with_summoner'],
        ))


@construction
def summoner_suggestions(request, query):
    matches = Summoner.objects.filter(name__istartswith=query)
    matches = matches.order_by('name').values_list('name', flat=True)[:5]
    return JsonResponse({'summoners': list(matches)})


@construction
def encounters(request):
    context = {}
    if request.method == 'POST':
        form = CompileEncountersForm(request.POST, request=request)
    else:
        form = CompileEncountersForm(request.GET or None, request=request)

    if form.is_valid():
        logger.info('form was valid')

    context['is_encounters'] = True
    context['summoner_form'] = form
    context['extra_scripts'] = form.extra_scripts()
    return render(request, 'encounters.html', context)


@construction
def xhr_summoners_encounters(request):
    if request.method == 'POST':
        form = CompileEncountersForm(request.POST, view=encounters)
    else:
        form = CompileEncountersForm(view=encounters)

    if form.is_valid():
        logger.info('form was valid')
        renderer = DejafedDuoEncounter(
            form.cleaned_data['summoner_a'],
            limit_encounters=form.cleaned_data['limit_encounters'],
            force_refetch=True,
            direct_link=form.direct_link(),
        )

        return HttpResponse(
            renderer.render_encounters_with(
                form.cleaned_data['summoner_b'],
            ),
        )
    return HttpResponse(status=500)


@require_POST
def set_colormode(request):
    modes = {
        'lightmode': {'add_class': None, 'remove_class': 'darkmode',
                      'new_label': 'dark', 'new_value': 'darkmode'},
        'darkmode': {'add_class': 'darkmode', 'remove_class': None,
                     'new_label': 'light', 'new_value': 'lightmode'},
    }

    desired_mode = request.POST.get('colormode')

    request.session['lightmode'] = desired_mode == 'lightmode'
    return JsonResponse(modes[desired_mode])
