from functools import wraps
from django.utils import timezone
from datetime import datetime
from datetime import timedelta
from time import sleep

from riotwatcher import LolWatcher
from riotwatcher import ApiError

from django.conf import settings
from django.db.models import Q

from inspect import getmembers
from inspect import ismethod

import logging


logger = logging.getLogger('dejafeddb')


def verbose_timing(fnc):
    @wraps(fnc)
    def wrapper(self, *args, **kwargs):
        start = datetime.now()
        result = fnc(self, *args, **kwargs)

        duration = datetime.now() - start
        logger.debug(f'after {duration}: {result}')
        return result
    return wrapper


def cache_file_name(region='', endpoint_name='', method_name='', url='',
                    ext='pickle'):
    raw = f'{region}_{endpoint_name}_{method_name}_{url}.{ext}'
    return ''.join([c for c in raw
                    if c.isalpha() or c.isdigit()
                    or c in {' ', '.', '_'}]).rstrip()


class _RiotApi(LolWatcher):
    _patchables = [
        'by_account',
        'by_id',
        'by_name',
        'by_puuid',
        'by_summoner',
        'by_summoner_by_champion',
        'by_team',
        'by_tournament',
        'challenger_by_queue',
        'entries',
        'grandmaster_by_queue',
        'masters_by_queue',
        'matchlist_by_puuid',
        'scores_by_summoner',
        'timeline_by_match',
        'tournaments',
        'tournamet_by_team',
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(settings.RIOT_API_KEY, *args, **kwargs)
        self.__used = 0

    def add_usage(self, klass=''):
        self.__used += 1
        logger.debug(f'API Usage (by {klass}): {self.__used}')

    def patch(self, obj, fnc_name, fnc):
        from dejafeddb.models import ApiLimit

        @wraps(fnc)
        def wrapper(*args, **kwargs):
            tries = 0
            result = None
            done = False
            last_err = None

            while not done and tries <= settings.API_429_RETRIES:
                wait_next = ApiLimit.objects.current()
                _now = timezone.now()
                if wait_next is not None and wait_next > timezone.now():
                    diff = wait_next - _now
                    logger.info(f'Waiting {diff.total_seconds()} in patched'
                                f' {fnc.__name__}')
                    sleep(diff.total_seconds())
                try:
                    result = fnc(*args, **kwargs)
                    ApiLimit.objects.reset()
                    done = True

                    logger.info(f'Finished patched {fnc.__name__}')

                except ApiError as err:
                    last_err = err
                    if not err.response.status_code == 429:
                        break
                    tries += 1
                    _wait = int(err.response.headers['Retry-After'])
                    next_try = timezone.now() + timedelta(seconds=_wait)
                    ApiLimit.objects.increase_to(next_try)

            if not done:
                raise last_err
            return result

        wrapper._is_patched = True

        setattr(obj, fnc_name, wrapper)

    def __getattribute__(self, item):
        if not item.startswith('_'):
            obj = super().__getattribute__(item)

            for name, method in getmembers(obj, predicate=ismethod):
                if getattr(method, '_is_patched', False):
                    continue

                if name in self._patchables:
                    logger.info(f'Patching {name} of '
                                f'{obj.__class__.__name__}.')
                    self.patch(obj, name, method)

            return obj
        return super().__getattribute__(item)


RiotApi = _RiotApi()


class SubmodelMapping(dict):
    pass


class RiotApiMixin:
    @verbose_timing
    def identifier_field(self, *args):
        raise NotImplementedError

    @verbose_timing
    def get_riot_api_data(self, *args):
        RiotApi.add_usage(self.__class__.__name__)

    @verbose_timing
    def api_mapping(self, *args, **kwargs):
        raise NotImplementedError

    @verbose_timing
    def get_or_create(self, identifier=None, **kwargs):
        logger.debug(
            f'in {self.__class__.__name__}: obj.get_or_create(identifier='
            f'{identifier}, kwargs={kwargs}',
        )
        try:
            obj = super().get(
                Q(**self.identifier_field(identifier)) | Q(**kwargs),
            )
            created = False
        except (self.model.DoesNotExist, IndexError):
            data = self.get_riot_api_data(identifier, **kwargs)
            if data is None:
                raise self.model.DoesNotExist

            mapped_data = self.api_mapping(data)

            sub_model_keys = []
            sub_models = {}
            for key in mapped_data:
                if isinstance(mapped_data[key], SubmodelMapping):
                    sub_model_keys.append(key)

            for key in sub_model_keys:
                sub_models[key] = mapped_data.pop(key)

            obj = super().create(**mapped_data)

            for key in sub_models:
                getattr(obj, key).create(**sub_models[key]).save()
            created = True

        return obj, created


def api_timestamp(data):
    result = None
    try:
        result = datetime.fromtimestamp(int(data) / 1000).astimezone()
    except OSError as e:
        logger.error(e)
    logger.info(f'Converting timestamp {data}: {result}')
    return result


class CachedApiResponse:
    class __CachedApiResponse:
        def __init__(self):
            self._cache = {}

        def _make_signature_str(self, fnc, args=(), kwargs={}):
            kwargs_str = '_'.join(
                '{}={}'.format(k, kwargs[k]) for k in sorted(kwargs.keys())
            )
            return (f'{fnc.__name__}_{"_".join(str(a) for a in args)}_'
                    f'{kwargs_str}')

        def get_cached(self, fnc, args=(), kwargs={},
                       deprecation=None):
            signature = self._make_signature_str(fnc, args, kwargs)

            if signature not in self._cache:
                def fetch():
                    logger.info(f'Updating cached response for {fnc.__name__}')
                    return fnc(*args, **kwargs)

                logger.info(f'Creating cached response for {fnc.__name__}')
                self._cache[signature] = {
                    '_update': fetch,
                    'data': fnc(*args, **kwargs),
                    'deprecation': deprecation or timedelta(days=1),
                    'fetched_at': timezone.now(),
                }

            _cache = self._cache[signature]

            if deprecation is not None:
                _cache['deprecation'] = deprecation

            planned_deprecation = _cache['fetched_at'] + _cache['deprecation']

            if planned_deprecation < timezone.now():
                _cache['data'] = _cache['_update']()
                _cache['fetched_at'] = timezone.now()

            return _cache['data']

    _instance = None

    def __init__(self):
        if CachedApiResponse._instance is None:
            _obj = CachedApiResponse.__CachedApiResponse()
            CachedApiResponse._instance = _obj

    def __setattr__(self, key, value):
        self._instance.__setattr__(key, value)

    def __getattr__(self, name):
        return getattr(self._instance, name)
