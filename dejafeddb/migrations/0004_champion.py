# Generated by Django 3.2.9 on 2021-12-06 00:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dejafeddb', '0003_summonermatch_championid'),
    ]

    operations = [
        migrations.CreateModel(
            name='Champion',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('asset_url', models.CharField(max_length=500)),
            ],
        ),
    ]
