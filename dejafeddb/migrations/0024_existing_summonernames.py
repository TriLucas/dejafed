from riotwatcher import ApiError
from django.db import migrations

from dejafeddb.api_utils import RiotApi
from dejafeddb.api_utils import api_timestamp


def create_data(apps, schema_editor):
    Summoner = apps.get_model('dejafeddb', 'Summoner')
    SummonerName = apps.get_model('dejafeddb', 'SummonerName')

    for summoner in Summoner.objects.all():
        try:
            api_data = RiotApi.summoner.by_puuid('euw1', summoner.puuid)

            kwargs = {
                'name': summoner.name,
                'for_summoner': summoner,
            }
            if api_data['name'] == summoner.name:
                kwargs['revision_date'] = api_timestamp(api_data['revisionDate'])
            else:
                SummonerName.objects.create(name=api_data['name'],
                                            for_summoner=summoner,
                                            revision_date=api_timestamp(
                                                api_data['revisionDate']
                                            ))

            SummonerName.objects.create(**kwargs)
        except ApiError:
            pass


def noop(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('dejafeddb', '0023_summonername'),
    ]

    operations = [
        migrations.RunPython(create_data, noop),
    ]
