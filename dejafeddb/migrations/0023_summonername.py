# Generated by Django 3.2.10 on 2022-02-13 04:42

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('dejafeddb', '0022_remove_summoner_dejafeddb_s_name_081710_idx'),
    ]

    operations = [
        migrations.CreateModel(
            name='SummonerName',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default=None, max_length=200)),
                ('revision_date', models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0, tzinfo=utc))),
                ('for_summoner', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='dejafeddb.summoner')),
            ],
        ),
    ]
