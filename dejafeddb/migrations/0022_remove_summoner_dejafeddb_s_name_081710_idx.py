# Generated by Django 3.2.10 on 2022-02-12 22:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dejafeddb', '0021_remove_summoner_last_match_start'),
    ]

    operations = [
        migrations.RemoveIndex(
            model_name='summoner',
            name='dejafeddb_s_name_081710_idx',
        ),
    ]
