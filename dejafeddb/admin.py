from django.contrib import admin

from .models import Switches
from .models import SummonerName
from .models import Summoner


@admin.register(Switches)
class AuthorAdmin(admin.ModelAdmin):
    list_display = 'name', 'switch'


class SummonerNameAdmin(admin.TabularInline):
    model = SummonerName

    readonly_fields = 'name', 'revision_date'
    extra = 0
    can_delete = False

    def has_add_permission(self, request, obj):
        return False


@admin.register(Summoner)
class SummonerAdmin(admin.ModelAdmin):
    readonly_fields = 'accountId', 'id', 'puuid'
    exclude = ('raw_past_matches', 'live_game_data',
               'raw_past_matches_fetched', 'live_game_data_fetched')

    inlines = SummonerNameAdmin,
    search_fields = ['name']
    list_display = 'display_name', 'puuid'
    list_display_links = 'display_name', 'puuid'
