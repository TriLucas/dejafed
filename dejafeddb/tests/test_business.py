
from unittest import mock
from itertools import product

from django.test import TestCase
from django.test import RequestFactory

from dejafeddb.utils import reverse_lazy

from .utils import primary_summoner
from .utils import make_encounters
from .utils import mocked_players
from .utils import mocked_last_match
from .utils import mocked_requests
from .utils import mocked_requests_static


@mock.patch('riotwatcher._apis.BaseApi.raw_request',
            side_effect=mocked_requests)
class LastMatchTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.factory = RequestFactory()

        from dejafeddb import views
        cls.views = views

        from dejafeddb.models import Summoner
        cls.Summoner = Summoner

        from dejafeddb.models import Switches
        Switches.objects.get_or_create(name='show_construction', switch=False)

        with mock.patch('riotwatcher._apis.BaseApi.raw_request_static',
                        side_effect=mocked_requests_static):
            from dejafeddb.management.commands import update_assets
            cmd = update_assets.Command()
            cmd.handle()

        make_encounters(*product((True, False), repeat=3))

    def test_0_get_or_create(self, *args, **kwargs):
        name = primary_summoner().name
        player, new = self.Summoner.objects.get_or_create(name)
        self.assertEqual(new, False)

        player2 = self.Summoner.objects.get(
            puuid=primary_summoner().puuid,
        )
        self.assertEqual(player, player2)

    def test_1_encounters_last_match(self, *args, **kwargs):
        primary = primary_summoner()
        response = self.client.post(reverse_lazy('index'),
                                    data={'name': primary.name,
                                          'match_select': 1,
                                          'limit_encounters': 20,
                                          'auto_refresh': 0})

        self.assertEqual(response.status_code, 200)

        n_encounters = 0
        for mocked_player in mocked_players():
            with self.subTest(f'Test encounter with {mocked_player.id}'):
                encounter_matches = primary.get_encounters_with(
                    mocked_player.puuid,
                )
                no_encounters = len(encounter_matches) == 0
                is_primary = mocked_player == primary

                if no_encounters or is_primary:
                    continue

                xhr_response = self.client.post(
                    reverse_lazy('xhr_current_encounter'),
                    data={'for_summoner': primary.id,
                          'with_summoner': mocked_player.id,
                          'limit_encounters': 5,
                          'match': mocked_last_match()},
                )

                self.assertEqual(xhr_response.status_code, 200)

                for entry in encounter_matches:
                    n_encounters += 1
                    self.assertContains(xhr_response, entry)
                    self.assertContains(response, entry)
        self.assertEqual(n_encounters, 8)

    def test_2_test_full_match(self, *args, **kwargs):
        response = self.client.get(reverse_lazy(
            'match',
            args=[mocked_last_match()],
        ))

        for player in mocked_players():
            with self.subTest(f'Test {player.id} in full match render'):
                self.assertContains(response, player.puuid)

    def test_3_encounterlist(self, *args, **kwargs):
        primary = primary_summoner()

        response = self.client.post(reverse_lazy('xhr_summoners_encounters'),
                                    data={'name_a': primary.name})
        self.assertEqual(response.status_code, 500)

        for player in mocked_players():
            encounters = primary.get_encounters_with(player.puuid)
            if primary == player or len(encounters) == 0:
                continue

            with self.subTest(f'Encounters {primary.name} and {player.name}'):
                response = self.client.post(
                    reverse_lazy('xhr_summoners_encounters'),
                    data={'name_a': primary.name,
                          'name_b': player.name,
                          'limit_encounters': 20},
                )
                for m_id in encounters:
                    self.assertContains(response, m_id)
