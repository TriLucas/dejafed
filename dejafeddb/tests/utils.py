import random
import uuid
import os
import json
import logging

from unittest import mock
from itertools import product

from datetime import datetime
from datetime import timedelta

from django.apps import apps
from django.core.exceptions import MiddlewareNotUsed
from django.conf import settings

from dejafeddb.models import Champion

from dejafeddb.api_utils import cache_file_name
from dejafeddb.api_utils import api_timestamp

random.seed(datetime.now())

logger = logging.getLogger('dejafeddb')


def _make_participant_data(lane, role, puuid, s_id, name, team=100, win=False,
                           p_id=1):

    champ_pks = Champion.objects.all().values_list('pk', flat=True)
    champ = Champion.objects.get(
        pk=champ_pks[random.randint(0, len(champ_pks) - 1)],
    )

    return {
        'assists': random.randint(0, 50),
        'championId': champ.id,
        'championName': champ.name,
        'deaths': random.randint(0, 50),
        'individualPosition': 'Invalid',
        'kills': random.randint(0, 50),
        'lane': lane,
        'participantId': p_id,
        'puuid': puuid,
        'role': role,
        'summonerId': s_id,
        'summonerName': name,
        'teamId': team,
        'teamPosition': '',
        'win': win,
        'champLevel': 1337,
        'summonerLevel': 1337,
        'item0': 1055,
        'item1': 1055,
        'item2': 1055,
        'item3': 1055,
        'item4': 1055,
        'item5': 1055,
        'item6': 1055,
    }


def _make_support(puuid, s_id, name, team=100, win=False, p_id=1):
    return _make_participant_data('UTILITY', 'SUPPORT', puuid, s_id, name,
                                  team, win, p_id)


def _make_carry(puuid, s_id, name, team=100, win=False, p_id=1):
    return _make_participant_data('BOTTOM', 'CARRY', puuid, s_id, name, team,
                                  win, p_id)


def _make_midlane(puuid, s_id, name, team=100, win=False, p_id=1):
    return _make_participant_data('MIDDLE', 'SOLO', puuid, s_id, name, team,
                                  win, p_id)


def _make_toplane(puuid, s_id, name, team=100, win=False, p_id=1):
    return _make_participant_data('TOP', 'SOLO', puuid, s_id, name, team, win,
                                  p_id)


def _make_jungle(puuid, s_id, name, team=100, win=False, p_id=1):
    return _make_participant_data('JUNGLE', 'NONE', puuid, s_id, name, team,
                                  win, p_id)


class SummonerMock:
    def __init__(self, name):
        self.name = name
        self.__puuid = None
        self.__id = None
        self.__account_id = None
        self._matches = {}

    @property
    def puuid(self):
        if self.__puuid is None:
            self.__puuid = uuid.uuid4().hex
        return self.__puuid

    @property
    def id(self):
        if self.__id is None:
            self.__id = uuid.uuid4().hex
        return self.__id

    @property
    def account_id(self):
        if self.__account_id is None:
            self.__account_id = uuid.uuid4().hex
        return self.__account_id

    def add_match(self, match_id, data):
        self._matches[match_id] = data

    def get_matches(self):
        return list(self._matches.keys())

    def get_match(self, match_id):
        return self._matches[match_id]

    def get_info(self):
        return {
            'id': self.id,
            'accountId': self.account_id,
            'puuid': self.puuid,
            'name': self.name,
            'profileIconId': 3231,
            'revisionDate': 1641566109605,
            'summonerLevel': 391,
        }


class PrimarySummonerMock(SummonerMock):
    def __init__(self, name):
        super().__init__(name)
        self._encounters = {}

    def add_encounter(self, foreign_id, match_id, match_data):
        self._encounters.setdefault(foreign_id, [])
        self._encounters[foreign_id].append(match_id)
        self._matches[match_id] = match_data

    def get_encounters_with(self, other_puuid):
        return self._encounters.get(other_puuid, [])


class SummonerMockPool:
    ROOT_MATCH = f'MOCK_{uuid.uuid4().hex[:15]}'

    def __init__(self, names=None):
        self._names = names or ['Hansi', 'Peter', 'Wolfgang', 'Paul', 'Katrin',
                                'Lucifer', 'Melcheor', 'Obama', 'Max',
                                'Herbert']

        self._players = [PrimarySummonerMock(self._names[0])]
        self._players += [SummonerMock(name) for name in self._names[1:]]
        self._puuids = [x.puuid for x in self._players]
        self._ids = [x.id for x in self._players]

        self.__last_participants = None

        self._matches = []
        self._match_counter = 0
        self._make_match_counter = 0

        self._is_initialized = False

    def _all_role_functions(self):
        return [_make_support, _make_carry, _make_jungle, _make_toplane,
                _make_midlane]

    def random_role_function(self):
        fncs = self._all_role_functions()
        return fncs[random.randint(0, len(fncs) - 1)]

    def _make_participants(self):
        self.__last_participants = []

        indices = list(range(0, 10))
        team_red_win = bool(random.randint(0, 1))

        fncs = self._all_role_functions() * 2

        for x in range(0, 10):
            _idx = random.randint(0, len(indices) - 1)
            p_idx = indices.pop(_idx)

            player = self._players[p_idx]

            data = fncs[x](
                player.puuid,
                player.id,
                player.name,
                team=100 if x <= 4 else 200,
                win=team_red_win ^ x <= 4,
                p_id=x + 1,
            )
            player.add_match(self.ROOT_MATCH, data)

            self.__last_participants.append((player.puuid, data))

    def _reset_pool(self):
        from dejafeddb.models import Summoner
        from dejafeddb.models import Match

        matches = []

        for player in self.players():
            for item in Summoner.objects.filter(name=player.name):
                for sm in item.summonermatch_set.all():
                    matches.append(sm.match.id)
                    sm.delete()

                item.summonername_set.delete()
                item.delete()

        Match.objects.filter(pk__in=matches).delete()

        self._players = [PrimarySummonerMock(self._names[0])]
        self._players += [SummonerMock(name) for name in self._names[1:]]
        self._puuids = [x.puuid for x in self._players]
        self._ids = [x.id for x in self._players]

        self.__last_participants = None

        self._matches = []
        self._match_counter = 0
        self._make_match_counter = 0

    @property
    def primary_summoner(self):
        if self.__last_participants is None:
            self._make_participants()
        return self._players[0]

    @property
    def last_participants(self):
        if self.__last_participants is None:
            self._make_participants()
        return self.__last_participants

    def make_encounter(self, is_ally=True, was_ally=True, they_won=True):
        p_team = self.primary_summoner.get_match(self.ROOT_MATCH)['teamId']
        possibles = [
            puuid for puuid, data in self.last_participants
            if (data['teamId'] == p_team) == is_ally
            and puuid != self.primary_summoner.puuid
        ]

        self._match_counter += 1
        new_match = f'MOCK_{uuid.uuid4().hex[:15]}'

        # Generate new match root primary
        new_own_team = [100, 200][random.randint(0, 1)]
        prim_data = self.random_role_function()(
            self.primary_summoner.puuid,
            self.primary_summoner.id,
            self.primary_summoner.name,
            team=new_own_team,
            win=was_ally and they_won,
            p_id=4,
        )

        # Find a encounter which matches the criteria
        puuid = possibles[random.randint(0, len(possibles) - 1)]
        player = self.player_by_puuid(puuid)

        # add the other to the new match
        other_team = 100 if new_own_team == 200 else 200
        data = self.random_role_function()(
            player.puuid,
            player.id,
            player.name,
            team=p_team if was_ally else other_team,
            win=they_won,
            p_id=5,
        )

        player.add_match(new_match, data)
        self.primary_summoner.add_encounter(player.puuid, new_match, prim_data)

    def make_encounters(self, *options):
        if self._is_initialized:
            self._reset_pool()

        self._is_initialized = True

        options = options or [(True, True, False)]
        for opt in options:
            self.make_encounter(*opt)

        from dejafeddb.models import Match
        from dejafeddb.models import Summoner
        from dejafeddb.models import SummonerName

        # Prepolate database

        matches = {}

        for summoner in self.players():
            s_obj = Summoner.objects.create(
                accountId=summoner.account_id,
                puuid=summoner.puuid,
                id=summoner.id,
                raw_past_matches=summoner.get_matches(),
                raw_past_matches_fetched=datetime.now().astimezone(),
            )
            s_obj.save()

            SummonerName.objects.create(
                name=summoner.name,
                for_summoner=s_obj,
            )

            for match in summoner.get_matches():
                matches.setdefault(match, [])
                matches[match].append(s_obj)

        for entry in matches:
            _raw = self.match_data(entry)
            obj = Match.objects.create(
                id=entry,
                mode=_raw['info']['gameMode'],
                date=api_timestamp(_raw['info']['gameStartTimestamp']),
                raw_data=_raw,
            )
            obj.save()
            obj.populate(with_summoners=matches[entry])

            logger.warning(f'N MATCHES NOW: {Match.objects.all().count()}')

    def player_by_puuid(self, puuid):
        return self._players[self._puuids.index(puuid)]

    def player_by_name(self, name):
        return self._players[self._names.index(name)]

    def matches_for_name(self, name):
        idx = self._names.index(name)
        return self._players[idx].get_matches()

    def matches_for_puuid(self, puuid):
        idx = self._puuids.index(puuid)
        return self._players[idx].get_matches()

    def matches_for_id(self, s_id):
        idx = self._ids.index(s_id)
        return self._players[idx].get_matches()

    def players(self):
        return self._players

    def _make_match_data(self, participants, match_id):
        self._make_match_counter += 1

        start = datetime.now() - timedelta(
            minutes=55 * self._make_match_counter,
        )
        end = datetime.now() - timedelta(
            minutes=30 * self._make_match_counter,
        )

        start = int(start.timestamp()) * 1000
        end = int(end.timestamp()) * 1000

        result = {
            'metadata': {
                'dataVersion': '2',
                'matchId': match_id,
                'participants': [],
            },
            'info': {
                'gameCreation': start,
                'gameDuration': end - start,
                'gameEndTimestamp': end,
                'gameId': match_id,
                'gameMode': 'CLASSIC',
                'gameName': 'teambuilder-match-5650895242',
                'gameStartTimestamp': start + 20,
                'gameType': 'MATCHED_GAME',
                'gameVersion': '12.1.416.4011',
                'mapId': 12,
                'participants': [],
                'tournamentCode': '',
            },
        }

        for puuid, data in participants:
            result['metadata']['participants'].append(puuid)
            result['info']['participants'].append(data)

        return result

    def match_data(self, match_id):
        participants = []
        for entry in self._players:
            try:
                participants.append((entry.puuid, entry.get_match(match_id)))
            except KeyError:
                pass
        return self._make_match_data(participants, match_id)


MOCK_POOL = SummonerMockPool()


def mocked_requests(endpoint='', method='', region='', url='', query_params={},
                    **kwargs):
    fname = cache_file_name(region, endpoint, method, url, ext='json')
    app_path = apps.get_app_config('dejafeddb').path
    full_file = os.path.join(app_path, 'tests', 'data', fname)

    if os.path.exists(full_file):
        with open(full_file, 'r') as fp:
            data = json.load(fp)
        return data

    if method == 'by_name' and endpoint == 'SummonerApiV4':
        name = url.split('/')[-1]
        return MOCK_POOL.player_by_name(name).get_info()

    if method == 'by_puuid' and endpoint == 'SummonerApiV4':
        puuid = url.split('/')[-1]
        return MOCK_POOL.player_by_puuid(puuid).get_info()

    if method == 'matchlist_by_puuid' and endpoint == 'MatchApiV5':
        puuid = url.split('/')[-2]
        if query_params.get('start', None) == 100:
            return []
        return MOCK_POOL.player_by_puuid(puuid).get_matches()

    if method == 'by_id' and endpoint == 'MatchApiV5':
        m_id = url.split('/')[-1]
        return MOCK_POOL.match_data(m_id)

    return {}


def mocked_requests_static(url, query_params):
    return mocked_requests(url=url, query_params=query_params)


def mocked_players():
    return MOCK_POOL.players()


def make_encounters(*options):
    MOCK_POOL.make_encounters(*options)


def primary_summoner():
    return MOCK_POOL.primary_summoner


def mocked_last_match():
    return MOCK_POOL.ROOT_MATCH


class DebugMockMiddleware:
    def __init__(self, get_reponse):
        if not settings.DEBUG_MOCK_RIOTAPI:
            raise MiddlewareNotUsed

        logger.info('Enabling API debug mocking.')
        self.get_response = get_reponse

        from dejafeddb.models import Summoner
        from dejafeddb.models import Match

        matches = []

        for player in MOCK_POOL.players():
            for item in Summoner.objects.filter(name=player.name):
                for sm in item.summonermatch_set.all():
                    matches.append(sm.match.id)
                    sm.delete()

                item.summonername_set.delete()
                item.delete()

        Match.objects.filter(pk__in=matches).delete()

        with mock.patch('riotwatcher._apis.BaseApi.raw_request',
                        side_effect=mocked_requests):
            make_encounters(*product((True, False), repeat=3))

    def __call__(self, request):
        with mock.patch('riotwatcher._apis.BaseApi.raw_request',
                        side_effect=mocked_requests):
            return self.get_response(request)
