
from unittest import mock
from itertools import product

from django.test import TestCase

from dejafeddb.utils import reverse_lazy

from .utils import make_encounters
from .utils import mocked_players
from .utils import mocked_requests
from .utils import mocked_requests_static


@mock.patch('riotwatcher._apis.BaseApi.raw_request',
            side_effect=mocked_requests)
class LastMatchTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        from dejafeddb.models import Switches
        Switches.objects.get_or_create(name='show_construction', switch=False)

        with mock.patch('riotwatcher._apis.BaseApi.raw_request_static',
                        side_effect=mocked_requests_static):
            from dejafeddb.management.commands import update_assets
            cmd = update_assets.Command()
            cmd.handle()

        make_encounters(*product((True, False), repeat=3))

    def test_0_suggestions(self, *args, **kwargs):
        for player in mocked_players():
            with self.subTest(f'Test sugestions for {player.name}'):
                response = self.client.get(reverse_lazy(
                    'xhr_summoner_suggestions',
                    args=[player.name[:4]],
                ))

                self.assertContains(response, player.name)
