import subprocess
import os

import logging

from functools import wraps

from unittest import mock
from itertools import product

from django.test import TestCase
from dejafeddb.utils import reverse_lazy

from .utils import primary_summoner
from .utils import make_encounters
from .utils import mocked_players
from .utils import mocked_requests
from .utils import mocked_requests_static

from datetime import datetime

logger = logging.getLogger('dejafeddb')


def needs_tidy(fnc):
    @wraps(fnc)
    def wrapper(obj, *args, **kwargs):
        if obj.tidy_installed:
            return fnc(obj, *args, **kwargs)
        obj.skipTest('Warning, "tidy" executable not found! Skipping..')
    return wrapper


@mock.patch('riotwatcher._apis.BaseApi.raw_request',
            side_effect=mocked_requests)
class LastMatchTestCase(TestCase):
    HTML_REPLACEMENTS = [
        ('&', '&amp;'),
        ('/static/dejafeddb',
         'file:///E:/dev/dejafed/dejafeddb/static/dejafeddb'),
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        try:
            subprocess.check_output(['tidy', '-h'], stderr=subprocess.STDOUT)
            cls.tidy_installed = True
        except FileNotFoundError:
            cls.tidy_installed = False

        if not os.path.exists('htmltests'):
            os.mkdir('htmltests')

        now_str = f'{datetime.now().timestamp()}'
        cls.html_out_dir = os.path.join('htmltests', now_str)

        if not os.path.exists(cls.html_out_dir):
            os.mkdir(cls.html_out_dir)

        from dejafeddb import views
        cls.views = views

        from dejafeddb.models import Summoner
        cls.Summoner = Summoner

        from dejafeddb.models import Switches
        cls.switch, _ = Switches.objects.get_or_create(
            name='show_construction',
            switch=False,
        )

        with mock.patch('riotwatcher._apis.BaseApi.raw_request_static',
                        side_effect=mocked_requests_static):
            from dejafeddb.management.commands import update_assets
            cmd = update_assets.Command()
            cmd.handle()

        make_encounters(*product((True, False), repeat=3))

    def _html_replacement(self, raw, idx=0):
        if idx < len(self.HTML_REPLACEMENTS):
            _left, _right = self.HTML_REPLACEMENTS[idx]
            return self._html_replacement(raw, idx + 1).replace(_left, _right)
        return raw

    def tidy_html_file(self, response, filename):
        tmp_html = os.path.join(self.html_out_dir, filename)
        with open(tmp_html, 'w') as fp:
            fp.write(self._html_replacement(response.content.decode('utf-8')))

        cmd = ['tidy', '-e', tmp_html]
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()

        combined = f'{stdout}{stderr}'.strip().rstrip()
        logger.warning(f'In {tmp_html}: {combined}')
        return f'{combined}'

    def assert_tidy_html(self, tidy_output, msg=None):
        with_warnings = ' 0 errors' in tidy_output
        no_warnings = 'No warnings or errors were found.' in tidy_output
        self.assertTrue(with_warnings or no_warnings, msg=tidy_output)

        if not with_warnings and not no_warnings:
            standard_msg = '"tidy" does not validate the provided HTML.'
            self.fail(self._formatMessage(msg, standard_msg))

    @needs_tidy
    def test_0_raw_index(self, *args, **kwargs):
        self.switch.switch = True
        self.switch.save()

        response = self.client.get(reverse_lazy('index'))
        tidy_output = self.tidy_html_file(response, 'index_construction.html')
        self.assert_tidy_html(tidy_output)

        self.assertContains(response, 'under construction!')

        self.switch.switch = False
        self.switch.save()

    @needs_tidy
    def test_1_raw_index(self, *args, **kwargs):
        response = self.client.get(reverse_lazy('index'))
        tidy_output = self.tidy_html_file(response, 'index_raw.html')
        self.assert_tidy_html(tidy_output)

    @needs_tidy
    def test_2_match_overview(self, *args, **kwargs):
        for mocked_player in mocked_players():
            with self.subTest(f'Test XHR encounter with {mocked_player.id}'):
                is_main = mocked_player == primary_summoner()
                q_data_url = reverse_lazy('index',
                                          qdict={'puuid': mocked_player.puuid,
                                                 'name': mocked_player.name,
                                                 'limit_encounters': 5,
                                                 'match_select': 1,
                                                 'auto_refresh': False})

                response = self.client.get(q_data_url)
                tidy_output = self.tidy_html_file(
                    response,
                    f'index_{mocked_player.name}'
                    f'{"_MAIN" if is_main else ""}.html',
                )
                self.assert_tidy_html(tidy_output)

    @needs_tidy
    def test_3_raw_encounters_of_two(self, *args, **kwargs):
        response = self.client.get(reverse_lazy('encounters'))
        tidy_output = self.tidy_html_file(response, 'encounters.html')
        self.assert_tidy_html(tidy_output)
