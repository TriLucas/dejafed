from dejafed.settings import *

MIGRATION_MODULES = {
    'auth': None,
    'contenttypes': None,
    'default': None,
    'sessions': None,
    'admin': None,
    'dejafeddb': None,
}

RIOT_API_KEY = 'XXXXXXXXXXXXXXXXXXXXX'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'handlers': [],
        'level': 'CRITICAL',
    },
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'test.sqlite3',
        'TEST': {
            'NAME': 'dejafed_test',
        },
    },
}

SHOW_CONSTRUCTION = False
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'
