from django.core.management.base import BaseCommand

import pickle
import json

from os import path
from os import walk


class Command(BaseCommand):
    help = 'Updates the static asset database.'

    def handle(self, *args, **options):
        for current_folder, folders, files in walk('testdata'):
            pickles = [f for f in files if path.splitext(f)[-1] == '.pickle']
            for item in pickles:
                with open(path.join(current_folder, item), 'rb') as fp:
                    response = pickle.load(fp)

                with open(path.join(current_folder,
                                    path.splitext(item)[0] + '.json'),
                          'w') as fp_out:
                    json.dump(json.loads(response.content), fp_out, indent=2)
