from django.core.management.base import BaseCommand
from dejafeddb.models import Champion

from dejafeddb.api_utils import RiotApiMixin
from dejafeddb.api_utils import RiotApi


class Command(BaseCommand, RiotApiMixin):
    help = 'Updates the static asset database.'

    def _get_data_version(self):
        return RiotApi.data_dragon.versions_for_region('euw1')['n']['champion']

    def _get_champions(self):
        return RiotApi.data_dragon.champions(self._get_data_version())

    def handle(self, *args, **options):
        new_champs = []
        champions = self._get_champions()
        for champ in champions['data']:
            _data = champions['data'][champ]
            pk = int(_data['key'])

            obj, _isnew = Champion.objects.get_or_create(id=pk, name=champ)
            if _isnew:
                new_champs.append(obj)
        if new_champs:
            self.stdout.write(self.style.SUCCESS(
                f'Added champions {", ".join(n.name for n in new_champs)}.',
            ))
