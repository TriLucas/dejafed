from django.core.management.base import BaseCommand

import os
import pickle
import json


class Command(BaseCommand):
    help = 'Updates the static asset database.'

    def handle(self, *args, **options):
        for current_folder, folders, files in os.walk('testdata'):
            jsons = [f for f in files if os.path.splitext(f)[-1] == '.json']
            for item in jsons:
                with open(os.path.join(current_folder, item), 'r') as fp:
                    data = json.load(fp)
                with open(os.path.join(current_folder,
                                       os.path.splitext(item)[0] + '.pickle'),
                          'rb') as fp:
                    response = pickle.load(fp)

                response._content = json.dumps(data).encode('utf-8')

                with open(os.path.join(current_folder,
                                       os.path.splitext(item)[0] + '.pickle'),
                          'wb') as fp:
                    pickle.dump(response, fp)
