from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

import sys


class Command(BaseCommand):
    help = 'Updates the static asset database.'

    def handle(self, *args, **options):
        if User.objects.filter(is_superuser=True).count() > 0:
            sys.exit(0)
        sys.exit(1)
