from django.apps import AppConfig


class DejafeddbConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dejafeddb'
