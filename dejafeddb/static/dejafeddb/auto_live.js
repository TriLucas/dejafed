"use strict";

window.addEventListener("load", event => {
  let refId = document.querySelector(".xhr_autorefresh_refid").value;
  if (refId == "") {
    refId = null;
  } else {
    refId = parseInt(refId, 10);
  }

  let refreshTime = document.querySelector(".xhr_autorefresh_time").value;
  let refreshUrl = document.querySelector(".xhr_autorefresh_url").value;


  let scheduleNext = fnc => {
    window.setTimeout(fnc, refreshTime);
  };

  let checkEqual = () => {
    let xhr = new XMLHttpRequest();

    xhr.onload = function(e) {
      let data = JSON.parse(this.responseText);
      let newId = data["gameId"];
      let newDatetime = data["start"];
      let nowDate = data["now"];

      let startSpan = document.querySelector("#id_span_current_timestamp");
      let lastRefresh = document.querySelector("#xhr_last_response");

      lastRefresh.style = "display: block;";
      lastRefresh.querySelector("span").innerText = nowDate;

      if (startSpan != null && newDatetime != null) {
        startSpan.innerHTML = newDatetime;
      }

      if (newId == refId) {
        scheduleNext(checkEqual);
      } else {
        let btnSelector = "#summoner_selection input[type=submit]";
        let btn = document.querySelector(btnSelector);
        btn.click();
      }
    };

    xhr.open("GET", refreshUrl);
    xhr.send();
  };

  scheduleNext(checkEqual);
});
