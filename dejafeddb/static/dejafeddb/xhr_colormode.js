"use strict";

window.addEventListener("load", event => {
  let getCookie = name => {
    let cookieValue = null;
    if (document.cookie && document.cookie !== "") {
      const cookies = document.cookie.split(";");
      for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i].trim();
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + "=")) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  };

  let setterElem = document.querySelector("#id_footer .set_color");
  let urlInput = document.querySelector("#id_footer .xhr_color_url");
  let modeInput = document.querySelector("#id_footer .xhr_color_mode");
  let colorLabel = document.querySelector("#id_footer .color_label");

  let rootBody = document.querySelector("body");

  setterElem.addEventListener("click", clickEvent => {
    let csrftoken = getCookie("csrftoken");

    let data = new FormData();
    data.append("colormode", modeInput.value);

    let xhr = new XMLHttpRequest();

    xhr.onload = function(e) {
      let responseData = JSON.parse(this.responseText);

      colorLabel.innerHTML = responseData["new_label"];
      modeInput.value = responseData["new_value"];

      if (responseData["remove_class"] != null) {
        rootBody.classList.remove(responseData["remove_class"]);
      }
      if (responseData["add_class"] != null) {
        rootBody.classList.add(responseData["add_class"]);
      }
    };

    xhr.open("POST", urlInput.value);
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.send(data);
  });
});
