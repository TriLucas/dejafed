"use strict";

let collapse = null;
let uncollapse = null;
let toggleAutoRefresh = null;

collapse = function(identifier) {
  let elem = document.getElementById(identifier + "_known_from_container");
  elem.style.display = "none";

  let controller = document.getElementById(identifier + "_control");
  controller.onclick = () => uncollapse(identifier);
  controller.value = ">>";
};

uncollapse = function(identifier) {
  let elem = document.getElementById(identifier + "_known_from_container");
  elem.style.display = "";

  let controller = document.getElementById(identifier + "_control");
  controller.value = "<<";
  controller.onclick = () => collapse(identifier);
};

toggleAutoRefresh = function(element) {
  let refreshCheckBock = document.getElementById("id_auto_refresh");
  if (element.value == 1) {
    refreshCheckBock.disabled = true;
  } else {
    refreshCheckBock.disabled = false;
  }
};

window.addEventListener("load", event => {
  let refreshCheckBock = document.getElementById("id_auto_refresh");
  let radioLastCurrent = document.getElementById("id_match_select_1");

  if (radioLastCurrent.checked) {
    refreshCheckBock.disabled = true;
  } else {
    refreshCheckBock.disabled = false;
  }
});
