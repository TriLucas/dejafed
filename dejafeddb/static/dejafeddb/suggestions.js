"use strict";

window.addEventListener("load", event => {
  let suggestionsXhr = (targetId, datalistId) => {
    let debumpTimer = null;

    let nameInput = document.getElementById(targetId);
    let xhrUrl = document.getElementById("id_xhr_suggestions_url").value;

    let dataList = document.getElementById(datalistId);

    let activeXhr = null;

    let clearSuggestions = () => {
      nameInput.setAttribute("list", "");
      nameInput.setAttribute("autocomplete", "off");
      dataList.innerHTML = "";
    };

    nameInput.addEventListener("input", inputEvent => {
      if (inputEvent.inputType != "insertText") {
        return;
      }

      if (nameInput.value.length > 3) {
        if (debumpTimer != null) {
          window.clearTimeout(debumpTimer);
          debumpTimer = null;
        }
        debumpTimer = window.setTimeout(timeoutEvent => {
          let url = xhrUrl.replace("SUMMONER_QUERY", nameInput.value);

          if (activeXhr != null) {
            activeXhr.abort();
          }
          activeXhr = new XMLHttpRequest();
          activeXhr.onload = function(eventLoad) {
            let data = JSON.parse(this.responseText);
            activeXhr = null;

            if (data["summoners"].length == 0) {
              clearSuggestions();
            } else {
              clearSuggestions();
              nameInput.setAttribute("list", datalistId);
              nameInput.setAttribute("autocomplete", "on");

              for (let entry of data["summoners"]) {
                let newOption = document.createElement("option");
                newOption.setAttribute("value", entry);
                dataList.appendChild(newOption);
              }
            }
          };
          activeXhr.open("GET", url);
          activeXhr.send();
        }, 400);
      } else {
        clearSuggestions();
      }
    });
  };

  let rootForm = document.querySelector("#summoner_selection");
  if (rootForm == null) {
    return;
  }

  let numTargetsInput = rootForm.querySelector(".n_suggestion_targets");
  if (numTargetsInput == null) {
    return;
  }

  let numTargets = parseInt(numTargetsInput.value, 10);

  for (let idx = 0; idx < numTargets; idx++) {
    let selectorTarget = ".suggestion_target_" + idx;
    let selectorDatalist = ".suggestion_datalist_" + idx;
    let targetId = rootForm.querySelector(selectorTarget).value;
    let datalistId = rootForm.querySelector(selectorDatalist).value;

    suggestionsXhr(targetId, datalistId);
  }
});
