"use strict";

let showControlContainer = containerId => {
  document.getElementById(containerId).style = "";
};

let getCookie = name => {
  let cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + "=")) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
};

let performXhrPost = event => {
  const csrftoken = getCookie("csrftoken");

  let targets = Array.prototype.slice.call(
    document.getElementsByClassName("xhr_form")
  );

  let next = null;

  let parseNext = () => {
    if (targets.length == 0) {
      return;
    }

    let nextTarget = targets.pop(0);
    let url = nextTarget.getElementsByClassName("xhr_url")[0].value;

    let targetId = nextTarget.getElementsByClassName("xhr_target")[0].value;

    let target = document.getElementById(targetId);
    target.innerHTML = "";

    let controlContainerId = null;
    let controlDefiner = nextTarget.getElementsByClassName("xhr_control")[0];
    if (controlDefiner != null) {
      controlContainerId = controlDefiner.value;
    }

    let spinnerId = nextTarget.getElementsByClassName("xhr_spinner")[0].value;
    let spinner = document.getElementById(spinnerId);
    spinner.style = "display: block;";

    next(nextTarget, url, targetId, controlContainerId, spinnerId);
  };

  next = (form, url, targetId, controlContainerId, spinnerId) => {
    let xhr = new XMLHttpRequest();

    let data = new FormData(form);

    xhr.onload = function(e) {
      let target = document.getElementById(targetId);
      let spinner = document.getElementById(spinnerId);
      spinner.style = "display: none;";
      target.innerHTML = this.responseText;
      if (this.responseText.length > 0 && controlContainerId != null) {
        document.getElementById(controlContainerId).style = "";
      }

      parseNext();
    };

    xhr.open("POST", url);
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.send(data);
  };

  parseNext();
};

window.addEventListener("load", event => {
  let autoSubmitter = document.querySelector(".xhr_auto_submit");
  let rootForm = document.querySelector("#summoner_selection");

  let invalidForm = ((rootForm == null) || (rootForm.className == "xhr_form"));

  if (invalidForm && autoSubmitter == null) {
    return;
  }

  performXhrPost();
});
