from urllib.parse import urlunparse
from urllib.parse import urlparse
from urllib.parse import urlencode

from django.utils.functional import lazy
from django.urls import reverse


def _queried_url(target, *args, qdict={}, **kwargs):
    root_url = reverse(target, *args, **kwargs)

    query = urlencode(qdict)

    return urlunparse(urlparse(root_url)._replace(query=query))


def reverse_lazy(target, *args, **kwargs):
    return lazy(_queried_url, str)(target, *args, **kwargs)


def team_color(team_id, other=False):
    team_id = int(team_id)

    mapping = {
        100: 'BLUE',
        200: 'RED',
    }

    if other:
        team_id = 100 if team_id == 200 else 200

    return mapping[team_id]
