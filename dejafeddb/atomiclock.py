from NamedAtomicLock import NamedAtomicLock
from threading import get_ident

import logging


logger = logging.getLogger('dejafeddb')


class NamedAtomicLockContext(NamedAtomicLock):
    def __enter__(self):
        if not self.acquire():
            raise TimeoutError('Acquiring atomic lock timed out.')
        logger.info(f'Acquired atomic lock in {get_ident()}.')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_tb is not None and exc_val is not None:
            logger.error('Error in atomic Lock:',
                         exc_info=(exc_type, exc_val, exc_tb))
        self.release()
        logger.info(f'Released atomic lock in {get_ident()}.')
        return True
