import bisect

from django.db.models import Q
from django.db.models import Case
from django.db.models import When
from django.db.models import Value
from django.db.models import F

from django.template import loader
from django.utils import timezone

from .models import Summoner
from .models import Match
from .models import SummonerMatch
from .models import Champion

from .forms import MatchEncounterQueryForm

from .api_utils import api_timestamp

import logging


logger = logging.getLogger('dejafeddb')


class CurrentMatch:
    def __init__(self, data={}):
        self._data = data
        self._timestamp = timezone.now()
        self._type = 'ARAM'
        self.live_id = self._data.get('gameId', None)

    def make_context(self):
        logger.debug(self._data)
        return {
            'type': self._data['gameMode'],
            'timestamp': api_timestamp(self._data['gameStartTime']),
            'live_id': self.live_id,
        }

    def render(self):
        template = loader.get_template('current_match.html')
        return template.render(self.make_context())


class LastMatch(CurrentMatch):
    def __init__(self, match_id):
        self._match_obj, _ = Match.objects.get_or_create(match_id)
        super().__init__()

    def make_context(self):
        return {
            'timestamp': self._match_obj.date,
            'type': self._match_obj.mode,
            'pk': self._match_obj.pk,
        }


class DejafedDuoEncounter:
    class NoMatchFound(Exception):
        pass

    class NeedsRefetch(Exception):
        pass

    def __init__(self, for_summoner, limit_encounters=None,
                 force_refetch=False, both_sms=True, direct_link=None):
        self._for_summoner = for_summoner
        self._limit_encounters = limit_encounters
        self._render_both_sms = both_sms
        self._direct_link = direct_link

        self.force_refetch = force_refetch

    def _ignored_matches(self):
        return set()

    def _known_refs(self, origin, other):
        return set(origin.summonermatch_set.filter(
            match__summonermatch__summoner=other,
        ).values_list('match__id', flat=True))

    def render_encounters_with(self, other_summoner, raise_fetch=False):
        if other_summoner == self._for_summoner:
            return ''

        if raise_fetch:
            pov_expired = self._for_summoner.expired_matches()
            other_expired = other_summoner.expired_matches()
            if self.force_refetch or pov_expired or other_expired:
                raise self.NeedsRefetch

        context = {'summonermatches': [], 'other': other_summoner}

        reference_matches = self._for_summoner.last_matches_cached(
            force_refetch=self.force_refetch,
        )
        sms = other_summoner.last_matches_cached(
            force_refetch=self.force_refetch,
        )

# Try inserting already known matches into the reference list
# mitigating https://github.com/RiotGames/developer-relations/issues/517
        known_a = self._known_refs(self._for_summoner, other_summoner)
        known_b = self._known_refs(other_summoner, self._for_summoner)
        known = known_a.union(known_b)

        for item in [item for item in known if item not in sms]:
            bisect.insort(sms, item)
        # Since sms is SORTED (in ascending order) after this operation and we
        # chose to process the Match-list order returned by the RiotApi
        # (a somewhat obscure descending order), we can not use sms as our
        # source of ordering any more.

        ref_set = set(reference_matches).union(known).difference(
            self._ignored_matches(),
        )

        encounter_matches = set(sms).intersection(ref_set)

        logger.warning(f'N encounters: {len(encounter_matches)}')

        shared_matches = [m for m in reference_matches
                          if m in encounter_matches]
        shared_matches = shared_matches[:self._limit_encounters]

        # Check if any Match needs to be fetched vie Riot API
        if raise_fetch:
            known_matches = Match.objects.filter(id__in=shared_matches)
            pov_matches = known_matches.filter(
                summonermatch__summoner=self._for_summoner,
            ).values_list('id', flat=True)
            other_matches = known_matches.filter(
                summonermatch__summoner=other_summoner,
            ).values_list('id', flat=True)
            known_matches = known_matches.values_list('id', flat=True)

            if set(known_matches) != set(shared_matches):
                logger.info('Refetching: Match completely unknown.')
                raise self.NeedsRefetch
            if set(pov_matches) != set(shared_matches):
                logger.info('Refetching: Match not associated with POV.')
                raise self.NeedsRefetch
            if set(other_matches) != set(shared_matches):
                logger.info('Refetching: Match not associated with OTHER.')
                raise self.NeedsRefetch

        for match_id in shared_matches:
            match, created = Match.objects.get_or_create(match_id)
            match.populate([self._for_summoner, other_summoner])

        sm_query = SummonerMatch.objects.filter(
            Q(match__id__in=encounter_matches)
            & Q(summoner__id=other_summoner.id)
            & Q(match__summonermatch__summoner__id=self._for_summoner.id),
        ).annotate(
            allied=Case(
                When(team=F('match__summonermatch__team'), then=Value(True)),
                default=Value(False),
            ),
            own_champ=F('match__summonermatch__champion'),
        ).order_by('-match__date')[:self._limit_encounters]

        sm_query = list(sm_query)

        wins_allies = SummonerMatch.objects.filter(
            id__in=[sm.id for sm in sm_query if sm.allied],
        ).filter(win=True).count()

        wins_enemies = SummonerMatch.objects.filter(
            id__in=[sm.id for sm in sm_query if not sm.allied],
        ).filter(win=True).count()

        games_enemies = SummonerMatch.objects.filter(
            id__in=[sm.id for sm in sm_query if not sm.allied],
        ).count()

        for item in sm_query:
            item.own_champ = Champion.objects.get(pk=item.own_champ)

        context['render_both_sides'] = self._render_both_sms
        context['summonermatches'] = sm_query
        context['allied_wins'] = wins_allies
        context['a_wins_enemies'] = wins_enemies
        context['games_enemies'] = games_enemies
        context['direct_link'] = self._direct_link
        if self._render_both_sms:
            context['summoner_a'] = self._for_summoner
            context['summoner_b'] = other_summoner
            template = loader.get_template('summoners_encounter_xhr.html')
        else:
            template = loader.get_template('encounter_xhr.html')
        return template.render(context)


class DejafedApi(DejafedDuoEncounter):
    def __init__(self, for_summoner, handled_match=None, auto_refresh=False,
                 **kwargs):
        super().__init__(for_summoner, both_sms=False, **kwargs)

        self._auto_refresh = auto_refresh

        self.__handled_match = handled_match
        self.__handled_match_set = handled_match is not None

        self._own_team = None

        self.__roster_data = None
        self.__roster = None

        self.__match_data = None
        self.__live_data = None

    def _last_summoner_match(self):
        return None

    @property
    def handled_match(self):
        if self.__handled_match is None and not self.__handled_match_set:
            self.__handled_match = self._last_summoner_match()
            self.__handled_match_set = True
        return self.__handled_match

    def _match_data(self):
        return CurrentMatch(self.live_data)

    @property
    def match_data(self):
        if self.__match_data is None:
            self.__match_data = self._match_data()
        return self.__match_data

    @property
    def live_data(self):
        if self.__live_data is None:
            self.__live_data = self._for_summoner.live_data_cached(
                'euw1',
                force_refetch=self.force_refetch,
            )

            if self.__live_data is None:
                raise self.NoMatchFound

        return self.__live_data

    def _ignored_matches(self):
        return set()

    def _loop_participants(self):
        live_ids = [p['summonerId'] for p in self.live_data['participants']]

        players = Summoner.objects.filter(id__in=live_ids)
        existing_ids = players.values_list('id', flat=True)
        missing = [puuid for puuid in live_ids if puuid not in existing_ids]

        for s_id in missing:
            Summoner.objects.get_or_create(id=s_id)

        players_dict = {x.id: x for x in players}

        for player_data in self.live_data['participants']:
            yield (
                players_dict[player_data['summonerId']],
                player_data,
                Champion.objects.get(id=player_data['championId']),
            )

    def get_source_roster(self):
        p_objs = []

        for player_obj, player_data, champion in self._loop_participants():
            player_obj.champion = champion
            if player_obj == self._for_summoner:
                player_obj.is_active_viewed = True
            else:
                player_obj.is_active_viewed = False

            team = player_data['teamId']
            player_obj._team = team
            if player_obj == self._for_summoner:
                self._own_team = team

            p_objs.append(player_obj)

        return p_objs

    @staticmethod
    def roster_sort_key(value):
        return value.name

    @property
    def roster_data(self):
        if self.__roster_data is None:
            self.__roster_data = self.get_source_roster()

        return self.__roster_data

    @property
    def blue_team(self):
        return sorted(
            [p for p in self.roster_data if p._team == 100],
            key=self.roster_sort_key,
        )

    @property
    def red_team(self):
        return sorted(
            [p for p in self.roster_data if p._team == 200],
            key=self.roster_sort_key,
        )

    def get_current_match(self):
        return None

    def _render_team(self, team):
        entries = []
        roster_template = loader.get_template('roster_entry.html')

        for entry in team:
            context = {
                'summoner': entry,
                'match_id': self.handled_match,
                'viewed': self._for_summoner,
                'match_select': int(self.handled_match is not None),
                'limit_encounters': self._limit_encounters,
                'auto_refresh': self._auto_refresh,
            }

            try:
                context['pre_rendered'] = self.render_encounters_with(
                    entry,
                    raise_fetch=True,
                )
                logger.info(f'Directly rendering encounters with {entry.name}')
            except self.NeedsRefetch:
                context['xhr_form'] = MatchEncounterQueryForm(data={
                    'match': self.handled_match,
                    'for_summoner': self._for_summoner,
                    'with_summoner': entry,
                    'limit_encounters': self._limit_encounters,
                })
                logger.info(f'Making XHR for {entry.name}.')

            entries.append(roster_template.render(context))

        return ''.join(entries)

    def render_red_team(self):
        return self._render_team(self.red_team)

    def render_blue_team(self):
        return self._render_team(self.blue_team)


class DejafedApiLastmatch(DejafedApi):
    def _last_summoner_match(self):
        logger.info(f'Force refetching matchlist for {self._for_summoner}')
        return self._for_summoner.last_matches_cached(force_refetch=True)[0]

    def _ignored_matches(self):
        return {self.handled_match}

    def _match_data(self):
        return LastMatch(self.handled_match)

    def _loop_participants(self):
        match, created = Match.objects.get_or_create(
            self.handled_match,
        )

        data = match.raw_data
        match.populate()

        logger.info(f'Fully populated player list for {match}')
        players = {x.puuid: x for x in match.summoner_set.all()}

        logger.info(f'Yielding for {match}')
        for player_data in data['info']['participants']:
            player_obj = players[player_data['puuid']]
            player_obj.match_data = match.summonermatch_set.get(
                summoner=player_obj,
            )
            yield (
                players[player_data['puuid']],
                player_data,
                Champion.objects.get(id=player_data['championId']),
            )
