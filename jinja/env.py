from django.utils import timezone
from django.contrib.staticfiles.storage import staticfiles_storage

from django.template.defaultfilters import timesince
from django.utils.formats import localize
from django.utils.formats import number_format

from jinja2 import Environment

from dejafeddb.utils import reverse_lazy
from dejafeddb.utils import team_color


def _localize(value):
    return localize(value, True)


def _number_format(value, decimal_pos=2, **kwargs):
    return number_format(value, use_l10n=True, decimal_pos=decimal_pos,
                         **kwargs)


def summoner_role(value, mode='CLASSIC'):
    if mode != 'CLASSIC':
        return 'Random'

    mapping = {
        'SOLO TOP': 'Toplane',
        'DUO TOP': 'Toplane',
        'SUPPORT UTILITY': 'Support',
        'CARRY BOTTOM': 'Carry',
        'SOLO MIDDLE': 'Midlane',
        'DUO MIDDLE': 'Midlane',
        'SUPPORT MIDDLE': 'Midlane',
        'NONE JUNGLE': 'Jungle',
    }
    return mapping.get(value, f'UNKNOWN: "{value}"')


def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse_lazy,
        'now': timezone.now,
    })

    env.filters['timesince'] = timesince
    env.filters['localize'] = _localize
    env.filters['number_format'] = _number_format
    env.filters['team_color'] = team_color
    env.filters['summoner_role'] = summoner_role
    return env
